# Using Git Bash

### Table of Contents

1. [Downloading Git Bash](#section1)
2. [Installing Git Bash Onto Your Windows Computer](#section2)
3. [Creating and Uploading SSH keys](#section3)
4. [Confirming that you are able to connect from Git Bash to gitlab.com](#section4)
5. [Initial pull of a Git repository to the local repository on your laptop](#section5)
6. [Subsequent pull and push between your laptop and gitlab.com](#section6)
7. [Further Reading](#section7)

### 1. Download Git Bash <a name="section1"></a>

Download Git Bash from [https://gitforwindows.org/](https://gitforwindows.org/)

### 2. Installing Git Bash Onto Your Windows Computer <a name="section2"></a>

After installing Git Bash and opening the application's terminal window, create a config file (substituting your own name and email address of course).

In Git Bash, type:

<pre>
cd ~
git config --global user.name "Syd Allan"
git config --global user.email SydAllan@gmail.com
git config --global core.autocrlf true
</pre>

This Git configuration information will be stored in `~/.gitconfig`.  It is possible to set these options differently in each repository (`--local` instead of `--global`).  Note that `~/.gitconfig` is a text file, so you can generate it with a script, or copy it from a central location rather than typing the git config commands if that is what you prefer.

In Git Bash, type:

<pre>cat ~/.gitconfig</pre>

Result:

<pre><i>[user]
        name = Syd Allan
        email = SydAllan@gmail.com
[credential]
        helper = wincred
[color]
        ui = auto
[core]
        autocrlf = true
        fileMode = false
</i></pre>

In Git Bash, type:

<pre>git config --global --list</pre>

Result:

<pre><i>user.name=Syd Allan
user.email=SydAllan@gmail.com
credential.helper=wincred
color.ui=auto
core.autocrlf=true
core.filemode=false
</i></pre>

There is a detailed outline of git configuration at [https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration](https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration) and a list of git configuration options at [https://git-scm.com/docs/git-config](https://git-scm.com/docs/git-config)

### 3. Creating and Uploading SSH keys <a name="section3"></a>

Before you connect to the Gitlab server from Git Bash for the first time you have to generate a pair of SSH keys and paste one of them in the Gitlab.com web page.

The overall mechanism of a pair of keys is that there is a private key that stays on your laptop, and a corresponding public key that goes onto a server you will be connecting to.  Something encrypted with the private key can be decrypted with the public key, and something encrypted with the public key can be decrypted with the private key.  So if a program on your laptop encrypts something with the private key and sends the encrypted file to the server, then the server will be able to decrypt it using the public key and thereby know that the encrypted file was sent from a laptop that contains the correct private key.  The same thing is true in reverse, and it allows your laptop to know that the server it is communicating with is the same server to which you uploaded the public key previously.  This allows 2 computers communicating using the SSH prototocol to be sure of each others' identity.

If you connect to multiple servers using SSH from your computer you might have several different SSH key-pairs.  So when you are prompted to name your keys, you might consider giving them a meaningful name that describes that they will be used to connect your specific computer to a specific Gitlab server.  The default key-pair name below is `id_ed25519` and the name that is specified to override that default is `syd_m00412105_gitlab_ed25519_220420`

The process that creates the pair of keys will prompt you for an optional password.  You can just press `Enter` twice, thereby specifying no password at all.  If you did provide a password you would have to type that password every time you sent or received anything from the Gitlab server.

In Git Bash, type:

<pre>
mkdir ~/.ssh
cd ~/.ssh
ssh-keygen -t ed25519 -C "SydAllan@gmail.com"
</pre>

Result:

<pre><i>Generating public/private ed25519 key pair.
Enter file in which to save the key (/c/Users/syd/.ssh/id_ed25519):</i> <b>/c/Users/syd/.ssh/syd_NUC_gitlab_ed25519_220420</b>
<i>Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /c/Users/syd/syd_NUC_gitlab_ed25519_220420.
Your public key has been saved in /c/Users/syd/syd_NUC_gitlab_ed25519_220420.pub.
The key fingerprint is:
SHA256:+vlKbtvawjFci8TFA4J+4qjetG0vbGVlI7olDQXpqd8 SydAllan@gmail.com
The key's randomart image is:</i>
+--[ED25519 256]--+
|   ...+o         |
|  .  o .+        |
| .  ..o. .       |
|  o .+o..+       |
| o o.o+oS..      |
|. .. o=*.        |
|.  ...Oo.        |
|. o.o*oE.+       |
|.. oo.o+O+o      |
+----[SHA256]-----+

</pre>

Type:

<pre>ls -la ~/.ssh</pre>

Result:

<pre><i>total 95
drwxr-xr-x 1 syd 1049089    0 Apr 20 18:07 ./
drwxr-xr-x 1 syd 1049089    0 Apr 20 18:10 ../
-rw-r--r-- 1 syd 1049089  244 Apr 20 11:07 config
-rw-r--r-- 1 syd 1049089  419 Apr 20 10:55 syd_NUC_gitlab_ed25519_220420
-rw-r--r-- 1 syd 1049089  110 Apr 20 10:55 syd_NUC_gitlab_ed25519_220420.pub
</i></pre>

Type:

<pre>cat ~/.ssh/syd_ed25519_gitlab_com_220420.pub</pre>

Result:

<pre><i>ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBe6UTadz0hBkCFS7ILPUXWF9pv2ldQ4qjiAMQcbbZnz SydAllan@gmail.com</i></pre>

In your browser, go to: [https://gitlab.com/-/profile/keys](https://gitlab.com/-/profile/keys)

Paste in the cat of `syd_ed25519_gitlab_com_220420.pub` from above into the `Key` box on the web page, in the `Title` box put a meaningful description (the key is wrapping in this image, but it was pasted as a single line, as shown in the `cat` above).

click `Add key`

In order to connect your copy of Git Bash to more than one git repository, add something similar to this in a file called `~/.ssh/config` (substitute your own key-file names for the part in <b><i>bold</i></b>):
<pre>
Host gitlab.com
   PreferredAuthentications publickey
   IdentityFile ~/.ssh/<b><i>syd_ed25519_gitlab_com_220420</i></b>
Host github.com/
   PreferredAuthentications publickey
   IdentityFile ~/.ssh/is_rsa
</pre>

### 4. Confirming that you are able to connect from Git Bash to gitlab.com <a name="section4"></a>

In Git Bash window type:

<pre>cd ~
git ls-remote git@gitlab.com:sydallan/public.git
</pre>

Result (you will type `yes` at the prompt):

<pre><i>The authenticity of host 'gitlab.com (172.65.251.78)' can't be established.
ECDSA key fingerprint is SHA256:HbW3g4zUjNSktFbqTiUWPWa2Bq1x8xdGUrliXFzSnUw.
Are you sure you want to continue connecting (yes/no)?</i> <b>yes</b>

<i>Warning: Permanently added 'gitlab.com,172.65.251.78' (ECDSA) to the list of known hosts.
7074c31ad0d27ca5ebbfea0caf2e0abefcab6fe5        HEAD
7074c31ad0d27ca5ebbfea0caf2e0abefcab6fe5        refs/heads/main
</i></pre>

The first time you connect to a server using SSH you will be prompted as shown above.  This means that the SSH public key for that host is not yet in your `~/.ssh/known_hosts` file.  When you answer `yes` above, the public key of that server is written in plain text in `~/.ssh/known_hosts` as one line like this:
<pre>cat ~/.ssh/known_hosts

<i>gitlab.com,172.65.251.78 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=
</i></pre>

Now that you have uploaded your SSH public key to gitlab.com, and you have allowed gitlab.com's public key to be added to your `~/.ssh/known_hosts` file, your computer will be able to decrypt messages from gitlab.com using gitlab.com's public key, and gitlab.com will be able to decrypt messages from your computer using your computer's public key.

Here are a couple of links to detailed documentation about how the process of generating SSH key pairs works:<br>
[https://www.ssh.com/academy/ssh/keygen](https://www.ssh.com/academy/ssh/keygen)<br>
[https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html](https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html)

### 5. Initial pull of a Git repository to the local repository on your laptop <a name="section5"></a>

Each person decides where they want to store their local copy of code for a project. Some people have been using `/z/gitlab`, e.g., storing code for the ABCD project in `/z/gitlab/ABCD`. Some of us have found it to be much faster to store our local git repositories on our `c:` drive, such as in `/c/users/<userid>/gitlab/`

To pull the code to your local folder the first time you use `git clone` in the Git Bash window. You need to go to the Gitlab browser to find the address for your project.

Copy the `Clone with SSH` address to your Windows clipboard.

The SSH cloning address for the repository at [https://gitlab.com/sydallan/public](https://gitlab.com/sydallan/public) for instance, is `git@gitlab.com:sydallan/public.git`

If you want to pull the ABCD project code down to your local folders, then this is what you do the first time (assume that you want the code to end up in `/c/users/<userid>/gitlab/abcd/repo`)

Open the Git Bash window by clicking the icon on your Windows start menu.  Type:
<pre>mkdir -p ~/gitlab/abcd
cd ~/gitlab/abcd
git clone git@gitlab.com:sydallan/public.git repo
</pre>

Result:

<pre><i>Cloning into 'repo'...
remote: Enumerating objects: 82, done.
remote: Counting objects: 100% (82/82), done.
remote: Compressing objects: 100% (75/75), done.
Receivinremote: Total 82 (delta 23), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (82/82), 625.89 KiB | 4.41 MiB/s, done.
Resolving deltas: 100% (23/23), done.
</i></pre>

You will then see that the folder `~/gitlab/abcd/repo` contains a sub-folder called `.git`  It is that `.git` folder that makes a folder a git repository.  If you delete that `.git` folder then you will just have a normal Windows folder full of code and documents and sub-folders.

You could also clone the repository into a folder 1 level higher (`~/gitlab/abcd` rather than `~/gitlab/abcd/repo`).  The reason we have been using a sub-folder called `repo` (it can be any name, but we have been using that name) is that this allows us to have a parent folder (`~/gitlab/abcd` in this example) that contains a repository (which is in the `repo` folder) as well as other files that are not in the repository.  But you can clone the repository into any folder you wish, using any naming convention you want.  The folder that ends up containing a sub-folder called `.git` is the folder that is your local repository.  Your repository can have as many sub-folders as you want, but there will be only 1 `.git` folder, and that will be in the top-level directory (`~/gitlab/abcd/repo/.git` in this case).

### 6. Subsequent pull and push between your laptop and gitlab.com <a name="section6"></a>

After you have used `git clone` to get your initial local copy of the repository, you can use the following command to retrieve updates from the remote repository at [https://gitlab.com/sydallan/public](https://gitlab.com/sydallan/public)
<pre>cd ~/gitlab/repo
git pull
</pre>

Note that when you first create your local repository using `git clone` you will be in a directory 1 level higher than when you subsequently run `git pull` (e.g. you run `git clone` while in `c:\Users\<userid>\gitlab\abcd` and you run `git pull` while in `c:\Users\<userid>\gitlab\abcd\repo`)

### 7. Further Reading <a name="section7"></a>

The best reference for git command syntax is at [https://git-scm.com/docs](https://git-scm.com/docs)

There are many commands of the form `git <command>` that can be typed into the Git Bash window.  But the main commands you use will probably be `pull`, `status`, `add`, `commit`, `push`, `checkout`, `tag`, and `diff`

